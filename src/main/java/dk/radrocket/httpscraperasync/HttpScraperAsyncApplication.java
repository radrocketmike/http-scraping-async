package dk.radrocket.httpscraperasync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HttpScraperAsyncApplication {

	public static void main(String[] args) {
		SpringApplication.run(HttpScraperAsyncApplication.class, args);
	}
}
