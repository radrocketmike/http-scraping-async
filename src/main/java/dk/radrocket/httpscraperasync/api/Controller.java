package dk.radrocket.httpscraperasync.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class Controller {


    private final String marsPath = "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=DEMO_KEY";
    private final String earthPath = "https://api.nasa.gov/planetary/earth/assets?lon=100.75&lat=1.5&begin=2014-02-01&api_key=DEMO_KEY";
    private final String asteroidPath = "https://ssd-api.jpl.nasa.gov/sentry.api";
    private URI earthUri;
    private URI marsUri;
    private URI asteroidUri;

    @Autowired
    RestTemplate restTemplate;

    public Controller() {
        try {
            earthUri = new URI(earthPath);
            marsUri = new URI(marsPath);
            asteroidUri = new URI(asteroidPath);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @GetMapping(path = "/api/getsync")
    public String getSync() {
        int startSecond = LocalTime.now().getSecond();

        int earthPayload = getPayload(earthUri);
        int marsPayload = getPayload(marsUri);
        int asteroidPayload = getPayload(asteroidUri);

        int spentTime = LocalTime.now().getSecond() - startSecond;
        int totalPayload = earthPayload + marsPayload + asteroidPayload;

        StringBuilder response = new StringBuilder();
        response.append("Spent " + spentTime + " seconds, ");
        response.append("fetching " + totalPayload + " characters.");

        return response.toString();
    }


    @GetMapping(path = "/api/getasync")
    public String getAsync() {

        int startSecond = LocalTime.now().getSecond();

        CompletableFuture<Integer> earthFuture = CompletableFuture.supplyAsync(() -> getPayload(earthUri));
        CompletableFuture<Integer> marsFuture = CompletableFuture.supplyAsync(() -> getPayload(marsUri));
        CompletableFuture<Integer> asteroidFuture = CompletableFuture.supplyAsync(() -> getPayload(asteroidUri));

        CompletableFuture<Void> combinedPayload = CompletableFuture.allOf(earthFuture, marsFuture, asteroidFuture);

        try {
            //Execute all calls asynchronously and await completion
            combinedPayload.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        int spentTime = LocalTime.now().getSecond() - startSecond;
        List<Integer> combined = Stream.of(earthFuture, marsFuture, asteroidFuture)
                .map(CompletableFuture::join)
                .collect(Collectors.toList());

        int totalCharacters = combined.stream().mapToInt(Integer::intValue).sum();

        StringBuilder response = new StringBuilder();
        response.append("Spent " + spentTime + " seconds, ");
        response.append("fetching " + totalCharacters + " characters.");



        return response.toString();

    }


    private int getPayload(URI uri) {
        ResponseEntity<String> response;
        response = restTemplate.getForEntity(uri, String.class);
        return response.getBody().length();
    }

}
